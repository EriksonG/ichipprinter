﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iChipPrinter.UI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PopulateInstalledPrintersComboBox();

            
        }

        private void PopulateInstalledPrintersComboBox()
        {
            //string selectedPrinter = Properties.Settings.Default.ticketPrinter;
            string installedPrinters;

            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                installedPrinters = PrinterSettings.InstalledPrinters[i];
                PrintersComboBox.Items.Add(installedPrinters);
            }

            /*if (selectedPrinter != "")
            {
                int index = PrintersComboBox.FindStringExact(selectedPrinter);
                PrintersComboBox.SelectedIndex = index;
            }*/
        }

        private void Write(string texto)
        {
            Quadro.Items.Add(new {Text = texto});
        }

        private static void PrintTicket(string data, string impresora)
        {
            PrinterSettings printJob = new PrinterSettings
            {
                PrinterName = impresora
            };

            if (!string.IsNullOrEmpty(data))
            {
                RawPrinterHelper.SendStringToPrinter(printJob.PrinterName, data);
            }
            Console.WriteLine("La respuesta esta vacia");
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Int32 port = 5000; //O cualquier otro siempre que no interfiera con los ya existentes
            IPAddress localAddr = IPAddress.Parse("127.0.0.1"); //Nos mantenemos a la escucha en "localhost"

            TcpListener server = null;
            server = new TcpListener(localAddr, port);

            server.Start(); //Nos mantenemos a la espera de nuevas peticiones
            Byte[] bytes = new Byte[1000]; //Array donde guardaremos el resultado
            String data = null; //Cadena de caracteres que contendrá los datos una vez procesados

            while (true)
            {
                Write("Waiting for a connection... ");
                TcpClient client = server.AcceptTcpClient(); //Aceptamos la conexión entrante
                Write("Connected!");
                //... //El resto del código dentro de este bucle

                NetworkStream stream = client.GetStream(); //Obtenemos el stream
                int i = stream.Read(bytes, 0, bytes.Length); //Leemos en el array "bytes" y almacenamos en i el numero de bytes leidos.
                data = System.Text.Encoding.ASCII.GetString(bytes, 0, i); //Convertimos la cadena
                Write($"Received: {data}"); //Mostramos por pantalla el resultado.

                //Dividimos el mensaje en un array de strings
                var lista = data.Split(' ');
                //Por si acaso la petición viene vacía.
                if (lista.Length < 3) continue;
                //El primer elemento de la lista será la instrucción
                var instruccion = lista[0];
                //El segundo elemento de la lista será la ruta
                var ruta = lista[1];
                //El tercer elemento antes del salto de carro, será el protocolo.
                string protocolo = lista[2].Split('\n')[0];
                //Finalmente mostramos los datos por pantalla
                Write($"Instruccion: {instruccion}\nRuta: {ruta}\nProtocolo: {protocolo}");

                var parametros = lista[1].Replace("/?", "").Split('&');

                var dic = new Dictionary<string, string>();

                foreach (var p in parametros)
                {
                    var dicValues = p.Split('=');

                    if (dicValues.Length > 1)
                        dic.Add(dicValues[0], dicValues[1]);

                    Write($"Llave: {dicValues[0]} - Valor: {dicValues[1]}");
                }
                /**/

                string html = string.Empty;
                string url = $@"https://www.google.com{lista[1]}";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.AutomaticDecompression = DecompressionMethods.GZip;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream streamResponse = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(streamResponse))
                {
                    html = reader.ReadToEnd();
                }

                PrintTicket(html, PrintersComboBox.Text);

                /*
                var uri = "";

                WebRequest request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                
                var order = new
                {
                    idFactura = 
                };

                var json_order = JsonConvert.SerializeObject(order, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                */


                /*
                byte[] msg;
                //Comprobamos que estemos recibiendo la peticion de la home
                if (ruta.Equals("/"))
                {
                    //Leemos todo el contenido del fichero especificado
                    var fichero = File.ReadAllText("home.html");
                    //Redactamos la cabecera de respuesta.
                    string response = "HTTP/1.1 200 OK\r\n\r\n\r\n";
                    //Agregamos a la cabecera la informacion del fichero.
                    response = response + fichero;
                    //Mostramos por pantalla el resultado
                    Console.WriteLine("Sent: {0}", response);
                    //Codificamos el texto que hemos cargado en un array de bytes
                    msg = System.Text.Encoding.ASCII.GetBytes(response);
                    //Escribimos en el stream el mensaje codiificado
                    stream.Write(msg, 0, msg.Length);
                }
                else
                {
                    //Redactamos una cabecera de fichero no encontrado
                    string response = "HTTP/1.1 404 Not Found";
                    //Mostramos por pantalla el resultado
                    Console.WriteLine("Sent: {0}", response);
                    //Codificamos, exactamente igual que en la parte superior
                    msg = System.Text.Encoding.ASCII.GetBytes(response);
                    //Escribimos en el stream el mensaje codificado
                    stream.Write(msg, 0, msg.Length);
                }
                */

                client.Close();
            }

        }
    }
}
